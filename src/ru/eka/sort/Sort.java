package ru.eka.sort;

import java.io.*;
import java.util.*;

/**
 * Класс для сортирвки элементов по частоте
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Sort {
    public static void main(String[] args) {
        String pathToFile = "src\\ru\\eka\\sort\\input.txt";
        String outPathToFile = "src\\ru\\eka\\sort\\output.txt";

        Integer[] inputArray  = strToNum(input(pathToFile).split(" "));
        Sort sort = new Sort(inputArray);
        sort.compute();
        write(sort.getResult(),outPathToFile);
        System.out.println(sort.getResult());
    }

    /**
     * Метод преобразования
     *
     * @param splitString массив String
     * @return массив Integer
     */
    private static Integer[] strToNum(String[] splitString){
       Integer[] result = new Integer[splitString.length];
       for (int i=0;i<splitString.length;i++){
           result[i] = Integer.parseInt(splitString[i]);
        }
        return result;
    }

    /**
     * Метод чтения из файла
     *
     * @param pathToFile путь до файла
     * @return считаные данные
     */
    private static String input(String pathToFile){
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))){
            return bufferedReader.readLine();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Метод записи в файл
     *
     * @param result данные для записи
     * @param outPathToFile путь до файла
     */
    private static void write(List<Integer> result, String outPathToFile) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(outPathToFile))) {
            try {
                output.write(result.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Integer> array;

    private Sort(Integer[] array) {
        this.array = Arrays.asList(array);
    }

    /**
     * Метод сортировки
     */
    private void compute() {
        array.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                int compareFreq = Collections.frequency(array, rhs) - Collections.frequency(array, lhs);
                return compareFreq != 0 ?
                        compareFreq :
                        rhs - lhs;
            }
        });
    }

    private List<Integer> getResult() {
        return array;
    }
}


